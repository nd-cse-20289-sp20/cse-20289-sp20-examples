#!/usr/bin/env python3

import os
import sys

try:
    pid = os.fork()                             # Discuss: fork()
except OSError:
    sys.exit(1)

if pid == 0:                                    # Discuss: Child Process
    print(f'Child pid: {os.getpid()}, Parent pid: {os.getppid()}')
    # Variant: Add sys.exit(1)
else:                                           # Discuss: Parent Process
    print(f'Parent pid: {os.getpid()}, Child pid: {pid}')
    try:
        pid, status = os.wait()                 # Discuss: wait()
    except OSError:
        pass

    print(f'Child pid: {pid}, Exit Status: {status}')
    # Variant: Use os.WEXITSTATUS
