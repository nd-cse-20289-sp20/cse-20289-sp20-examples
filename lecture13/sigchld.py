#!/usr/bin/env python3

import os
import signal
import sys
import time

# Globals

ChildPid    = 0
ChildStatus = None

# Functions

def handler(signum, frame):                     # Discuss: handler
    global ChildPid, ChildStatus                # Discuss: Global variables
    ChildPid, ChildStatus = os.wait()

# Main Execution

try:
    pid = os.fork()
except OSError:
    sys.exit(1)

if pid == 0:                                    # Review: Child Process
    print(f'Child pid: {os.getpid()}, Parent pid: {os.getppid()}')
    time.sleep(5)
else:                                           # Review: Parent Process
    signal.signal(signal.SIGCHLD, handler)      # Discuss: Registering signal

    while not ChildPid:                         # Discuss: Busy wait
        print(f'Parent pid: {os.getpid()}, Child pid: {pid}')
        time.sleep(1)

    print(f'Child pid: {ChildPid}, Exit Status: {os.WEXITSTATUS(ChildStatus)}')
