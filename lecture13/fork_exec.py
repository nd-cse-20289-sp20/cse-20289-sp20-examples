#!/usr/bin/env python3

import os
import sys

try:
    pid = os.fork()
except OSError:
    sys.exit(1)

if pid == 0:                                    # Review: Child Process
    print(f'Child pid: {os.getpid()}, Parent pid: {os.getppid()}')
    try:
        os.execlp('uname', 'uname', '-a')       # Discuss: execlp
    except OSError:
        sys.exit(1)                             # Discuss: sys.exit()
    # Variant: First argument is path, while second is the name of the process
    # Variant: Replace execlp with execvp
else:                                           # Review: Parent Process
    print(f'Parent pid: {os.getpid()}, Child pid: {pid}')
    try:
        pid, status = os.wait()                 
    except OSError:
        pass

    print(f'Child pid: {pid}, Exit Status: {os.WEXITSTATUS(status)}')
