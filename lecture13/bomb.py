#!/usr/bin/env python3

import os
import sys
import time

threshold = int(sys.argv[1])

for i in range(threshold):
    try:
        pid = os.fork()                         # Review: fork
    except OSError as e:                        # Discuss: exception object
        print(f'Fork Failure: {e}')     
        sys.exit(1)

    time.sleep(2)                               # Demo: watch pstree -u $USER
