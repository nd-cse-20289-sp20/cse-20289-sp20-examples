#!/usr/bin/env python3

import os
import sys
import time

nchildren = int(sys.argv[1])

for i in range(nchildren):
    try:
        pid = os.fork()                         # Review: fork()
    except OSError:
        sys.exit(1)

    if pid == 0:                                # Review: Child
        print(f'Child pid: {os.getpid()}, Parent pid: {os.getppid()}')
        sys.exit(i)

for i in range(nchildren):                      # Review: system
    os.system("ps ux | grep 'Z+' | grep -v grep")
    time.sleep(1)

    try:
        pid, status = os.wait()                 # Discuss: cleanup child
    except OSError:
        pass

    print(f'Child pid: {pid}, Exit Status: {os.WEXITSTATUS(status)}')
