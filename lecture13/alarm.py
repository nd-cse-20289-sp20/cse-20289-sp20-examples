#!/usr/bin/env python3

import signal
import time

# Global

Stop = False

# Handler

def alarm_handler(signum, frame):
    global Stop                                 # Discuss: Global variables

    print("Time's Up!")
    Stop = True                                 # Discuss: Global variables

# Main Execution

signal.signal(signal.SIGALRM, alarm_handler)    # Review: Registering signal
signal.alarm(10)                                # Discuss: Setting alarm

while not Stop:
    print('Waiting...')
    time.sleep(1)                               # Review: Sleeping

print('Finished!')

