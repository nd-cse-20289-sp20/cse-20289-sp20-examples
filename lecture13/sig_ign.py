#!/usr/bin/env python3

import os
import sys
import time
import signal

nchildren = int(sys.argv[1])

signal.signal(signal.SIGCHLD, signal.SIG_IGN)   # Discuss: Ignoring signals

for i in range(nchildren):
    try:
        pid = os.fork()                         # Review: fork()
    except OSError:
        sys.exit(1)

    if pid == 0:                                # Review: Child
        print(f'Child pid: {os.getpid()}, Parent pid: {os.getppid()}')
        sys.exit(i)

for i in range(nchildren):                      # Discuss: Lack of zombiles
    os.system("ps ux | grep 'Z+' | grep -v grep")
    time.sleep(1)
