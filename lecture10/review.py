#!/usr/bin/env python3

# Imperative Programming

numbers = range(0, 10)

doubled = []                                    # Discuss: apply operation
for n in numbers:                               # to each element
    doubled.append(n * 2)

selected = []                                   # Discuss: check operation
for d in doubled:                               # on each element
    if not d % 4:                               # Discuss: not
        selected.append(d)

for s in selected:
    print(s)

# Functional Programming

numbers  = range(0, 10)
doubled  = map(lambda n: n * 2, numbers)        # Review: map, lambda
selected = filter(lambda d: not d % 4, doubled) # Review: filter

for s in selected:                              # Warning: generator
    print(s)

# List Comprehension

numbers  = range(0, 10)
doubled  = [n * 2 for n in numbers]             # Review: list comprehension
selected = [d for d in doubled if not d % 4]

for s in selected:
    print(s)
