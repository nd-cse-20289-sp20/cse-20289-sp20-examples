#!/usr/bin/env python3

import sys

limit = int(sys.argv[1])        # Try with 10000000

# List

numbers = list(range(limit))    # Materialize list
for index in numbers:
    print(index)
