#!/usr/bin/env python3

import sys

limit = int(sys.argv[1])        # Try with 10000000

# Iterator/Generator

numbers = range(limit)          # Use iterator/generator
for index in numbers:
    print(index)
