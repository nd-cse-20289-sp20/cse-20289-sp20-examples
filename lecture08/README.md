# Examples 08

# Pipeline: Powerful Pattern

Three volunteers to role-play and demonstrate a pipeline.

1. How many Starbursts are pink?

    read | filter pink | count

2. How many different colors of Starbursts do we have?

    read | sort | uniq

# Pipeline: Demonstration

1. How many instances of bash are running?

        $ ps aux | grep bash | grep -v grep | wc -l

2. How many different types of shells are being used?

        $ ps aux | grep -Ev '(grep|ssh|flush)' | grep -Eo '(bash|csh|tcsh)' | sort | uniq

    To see how many of each, do `uniq -c`

3. Who has the most processes?

        for user in $(ps aux | cut -d ' ' -f 1 | sort | uniq); do
            echo $(ps aux | grep -v grep | grep ^$user | wc -l) $user
        done | sort -rn

    Alternatively:

        ps aux | cut -d ' ' -f 1 | sort | uniq -c | sort -rn

4. Who has the most TROLL/sleep processes?

        ps aux | grep TROLL | cut -d ' ' -f 1 | sort | uniq -c | sort -rn

# Regular Expressions: Syntax


        $ echo "Molly" | grep -E '.*'                 # Match all letters

	$ echo "Molly" | grep -E 'l*'                 # Match zero or more L's

	$ echo "Yeon"  | grep -E 'o?'                 # Match zero or one O's


	$ echo "Yeoon" | grep -E 'o{2}'               # Match exactly 2 O's


	$ echo "Yeoon" | grep -E '[eo]*'              # Match all O's or E's

	$ echo "Yeoon" | grep -E '[on]+'              # Match one or more of either O or N

	$ echo "Yeoon" | grep -E '[^on]+'             # Match everything but O or N

	$ echo "Yeoon" | grep -E '^Y'                 # Match start 

	$ echo "Yeoon" | grep -E 'n$'                 # Match end

	$ echo "Yeono" | grep -E '(o).*\1'            # Match with group references

# Regular Expressions: Examples

1. All the strings                                      .*

2. Only charmander and chespin                          ^[ch]

3. All words with two t's                               t{2}

4. Words that don't start with a vowel                  ^[^aeiou]

5. All words with two consecutive vowels                [aeiou]t

6. All words with two consecutive letters (same)        (.)\1

7. All words that begin and end with the same letter    ^([a-z]).*\1$

8. All words with exactly 2 of r, s, or t               '^[^rst]*[rst][^rst]*[rst][^rst]*$'

# Activity: Contact Harvesting

1. Extract all the phone numbers

        $ curl -sL "http://yld.me/aBG"
                | grep -Eo '[0-9]{3}-[0-9]{3}-[0-9]{4}' | sort | uniq

2. Extract all the phone numbers

        $ curl -sL "http://yld.me/aBG"
                | grep -Eo '[[:alnum:]]+@[[:alnum:].]+' | sort | uniq

3. Extract all the "Professorial" positions

        $ curl -sL "http://yld.me/aBG"
                | grep -Eo '[^>]+Prof[^<]+' | sort | uniq

# Activity: Faculty Information

1. How many faculty have Ph.D's? M.S.'s?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Ph\.D|M\.S).*/\1/p'
                | sort | uniq -c

        $ curl -sL http://cse.nd.edu/people/faculty
                | awk 'match($0, /(M\.S|Ph\.D)/, m) {counts[m[1]]++}
                  END { for (count in counts) { print counts[count], count } }'

2. How many faculty have Ph.D's or M.S's from Notre Dame?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Ph\.D|M\.S).*Notre Dame.*/\1/p'
                | sort | uniq -c

3. What year did most of the faculty graduate?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Ph\.D|M\.S).*([0-9]{4}).*/\2/p'
                | sort | uniq -c | sort -rn

4. How many professional specialists?

        $ curl -sL http://cse.nd.edu/people/faculty
                | sed -nr 's/.*(Professional Specialist).*/\1/p'
                #| grep -Eo 'Professional Specialist'
