#!/usr/bin/env python3

import os
import pprint
import requests

url      = 'https://reddit.com/r/linux/.json'   # Discuss: reddit API
headers  = {'user-agent': 'reddit-{}'.format(os.environ.get('USER', 'cse-20289-sp20'))}
response = requests.get(url, headers=headers)   # Review: reqeusts.get, headers
data     = response.json()                      # Review: JSON

#pprint.pprint(data)                            # Demo: exploring data using shell
#pprint.pprint(data['data'])                    
#pprint.pprint(data['data']['children'])
#pprint.pprint(data['data']['children'][0])

def get_score(article):
    return article['data']['score']

articles = data['data']['children']             # Discuss: sorting, lambda
#articles = sorted(articles, key=get_score, reverse=True)
articles = sorted(articles, key=lambda a: a['data']['score'], reverse=True)
articles = articles[:10]                        # Review: slicing

for index, article in enumerate(articles, 1):   # Review: enumerate
    title = article['data']['title']
    score = article['data']['score']

    print(f'{index:>7}. {title} ({score})')
