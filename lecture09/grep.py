#!/usr/bin/env python3

import os
import re
import sys

# Global variables

LINE_NUMBERS = False

# Usage function

def usage(status=0):
    print('''Usage: {} REGEX
    -n      Display line number'''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

# Parse command line options

arguments = sys.argv[1:]
while len(arguments) and arguments[0].startswith('-'):
    arg = arguments.pop(0)
    if arg == '-n':
        LINE_NUMBERS = True
    elif arg == '-h':
        usage(0)
    else:
        usage(1)

if len(arguments) != 1:
    usage(1)

REGEX = arguments[0]

# Main execution

for index, line in enumerate(sys.stdin):
    if re.search(REGEX, line):
        if LINE_NUMBERS:
            print('{:4}: {}'.format(index + 1, line), end='')
        else:
            print(line, end='')

