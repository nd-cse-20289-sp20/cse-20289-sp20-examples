#!/usr/bin/env python3

import sys

# Functions

def reverse_string(string):         # Discuss: docstring, doctest
    ''' Use a stack to reverse given string

    >>> reverse_string('abcdef')
    'fedcba'
    
    >>> reverse_string('yossarian lives')
    'sevil nairassoy'
    '''
    stack = []

    for letter in string:
        stack.append(letter)        # Discuss: Push letter to top of string

    result = ''
    while stack:
        result += stack.pop()       # Discuss: Pop letter from top of string

    return result

def main():
    for line in sys.stdin:
        line = line.rstrip()        # Discuss: Stripping trailing whitespace
        print(reverse_string(line))

# Main Execution

if __name__ == '__main__':          # Discuss: import guard
    main()                          # Demonstrate: module vs script
